# ProEvent project

## Project for coursework

** Щоб скачати та запустити: **

- Спочатку відкрийте консоль та введіть ** `git clone https://kiritoa@bitbucket.org/kiritoa/andriicoursework.git` **
- Потім перейдіть в папку з проектом та введіть в консоль ** `npm install` ** .
- Після завершення скачування пакетів потрібно ввести команду ** ` npm run start ` ** .

P.S: Спочатку переконайтеся, що маєте інстальований **Node.js** та **Git** на ПК